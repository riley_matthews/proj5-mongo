"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetsdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


@app.route("/input_error")
def _input_error():
    return flask.render_template('input_error.html')

@app.route("/submit_error")
def _submit_error():
    return render_template('submit_error.html')

@app.route("/display_error")
def _display_error():
    return render_template('display_error.html')

@app.route("/form_entry_error")
def _form_entry_error():
    return render_template('form_entry_error.html')


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_distance = request.args.get('brevet_dist', type=float)
    start_date = request.args.get('start_date', type=str)
    start_time = request.args.get('start_time', type=str)

    # make sure the string is in ISO 8601 format
    brevet_start_time = start_date + "T" + start_time + ":00+00:00"

    # debug output
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    # calculate open and close times. return json result
    open_time = acp_times.open_time(km, brevet_distance, brevet_start_time)
    close_time = acp_times.close_time(km, brevet_distance, brevet_start_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

###############

###############
#
# routes for the submit and display buttons
#
###############

@app.route("/new", methods=['POST'])
def _new():
    """
    The user has submitted the form.
    We get the values we want from the form-data and form
    json entrys with it. These entries are then inserted into
    the database.
    """
    flag = 0
    # get form-data
    data = request.form
    distance = data.get('distance')
    km = data.getlist('km')
    open_times = data.getlist('open')
    close_times = data.getlist('close')
    
    # check if all the entries are blank
    if (all(x == "" for x in km)):
        return redirect(url_for('_submit_error'))

    # check if the last control point is long enough
    for i in range(len(km)):
        if (km[i] == ""):
            continue
        if (int(km[i]) >= int(distance)):
            flag = 1

    if (flag == 0):
        return redirect(url_for('_form_entry_error'))

    # clear existing database entries
    db.brevetsdb.remove({})
    
    # insert into the database
    for i in range(len(km)):
        if (km[i] == ""):
            continue
        entry = {
            'distance': distance,
            'control_point': km[i],
            'open': open_times[i],
            'close': close_times[i]
        }

        db.brevetsdb.insert(entry)

    return redirect(url_for('index'))


@app.route("/display")
def _display():
    """
    The user has selected the display button.
    Get the database entries and pass them to the display page.
    """
    _items = db.brevetsdb.find()

    items = [item for item in _items]
    
    if (len(items) == 0):
        return redirect(url_for('_display_error'))
    
    return render_template('display.html', items=items)

###############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
