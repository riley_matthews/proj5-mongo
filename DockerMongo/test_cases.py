"""
Nose tests for acp_times.py
"""

from acp_times import *
import arrow

import nose
import logging

# test cases

def test_control_point_0():
	"""
	test cases for when the control point is located at 0km.
	The opening time should be the starting time of the brevet.
	The closing time by default is one hour after the starting time.
	"""
	start_time = arrow.get('2020-05-14T00:00:00-07:00')

	assert (open_time(0, 200, start_time) == start_time.isoformat())
	assert (open_time(0, 300, start_time) == start_time.isoformat())
	assert (open_time(0, 400, start_time) == start_time.isoformat())
	assert (open_time(0, 600, start_time) == start_time.isoformat())
	assert (open_time(0, 1000, start_time) == start_time.isoformat())

	assert (close_time(0, 200, start_time) == start_time.shift(hours=1).isoformat())
	assert (close_time(0, 300, start_time) == start_time.shift(hours=1).isoformat())
	assert (close_time(0, 400, start_time) == start_time.shift(hours=1).isoformat())
	assert (close_time(0, 600, start_time) == start_time.shift(hours=1).isoformat())
	assert (close_time(0, 1000, start_time) == start_time.shift(hours=1).isoformat())


def test_control_point_over_max():
	"""
	test cases for when the control point is at a distance larger than the brevet distance.
	From the rusa.org/pages/orgreg website:
	 	Opening: "the opening time for the finish checkpoint is calculated in the same way as those for the intermediate
	 	checkpoint, however the theoretical distance (200, 300, 400, 600 KM (1000)) is used."

	 	Closing: "The closing time for the finsih checkpoint is calculated by adding the maximum permitted time for the 
	 	brevet to the opening time of the start checkpoint. MAximum permitted time are 13:30 for 200 KM, 20:00 for 300Km
	 	27:00 for 400 KM, 40:00 for 600KM and 75:00 for 1000KM."
	"""

	start_time = arrow.get('2020-05-14T00:00:00-07:00')

	assert (open_time(215, 200, start_time) == '2020-05-14T05:53:00-07:00')
	assert (open_time(315, 300, start_time) == '2020-05-14T09:00:00-07:00')
	assert (open_time(415, 400, start_time) == '2020-05-14T12:08:00-07:00')
	assert (open_time(615, 600, start_time) == '2020-05-14T18:48:00-07:00')
	assert (open_time(1015, 1000, start_time) == '2020-05-15T09:05:00-07:00')

	assert (close_time(215, 200, start_time) == start_time.shift(hours=13, minutes=30).isoformat())
	assert (close_time(315, 300, start_time) == start_time.shift(hours=20).isoformat())
	assert (close_time(415, 400, start_time) == start_time.shift(hours=27).isoformat())
	assert (close_time(615, 600, start_time) == start_time.shift(hours=40).isoformat())
	assert (close_time(1015, 1000, start_time) == start_time.shift(hours=75).isoformat())


def test_decimal():
	"""
	Control times with a decimal part are rounded to the nearest integer before calculating times.
	"""

	start_time = arrow.get('2020-05-14T00:00:00-07:00')

	assert (open_time(95.4, 200, start_time) == open_time(95, 200, start_time) == '2020-05-14T02:48:00-07:00')
	assert (open_time(95.5, 200, start_time) == open_time(96, 200, start_time) == '2020-05-14T02:49:00-07:00')

	assert (close_time(95.4, 200, start_time) == close_time(95, 200, start_time) == '2020-05-14T06:20:00-07:00')
	assert (close_time(95.5, 200, start_time) == close_time(96, 200, start_time) == '2020-05-14T06:24:00-07:00')


def test_example_1():
	"""
	A collection of examples from: https://rusa.org/pages/acp-brevet-control-times-calculator
	"""
	start_time = arrow.get('2020-05-14T00:00:00-07:00')

	assert (open_time(60, 200, start_time) == '2020-05-14T01:46:00-07:00')
	assert (open_time(120, 200, start_time) == '2020-05-14T03:32:00-07:00')
	assert (open_time(175, 200, start_time) == '2020-05-14T05:09:00-07:00')
	assert (open_time(205, 200, start_time) == '2020-05-14T05:53:00-07:00')
	assert (open_time(550, 600, start_time) == '2020-05-14T17:08:00-07:00')
	assert (open_time(890, 1000, start_time) == '2020-05-15T05:09:00-07:00')

	assert (close_time(60, 200, start_time) == '2020-05-14T04:00:00-07:00')
	assert (close_time(120, 200, start_time) == '2020-05-14T08:00:00-07:00')
	assert (close_time(175, 200, start_time) == '2020-05-14T11:40:00-07:00')
	assert (close_time(205, 200, start_time) == '2020-05-14T13:30:00-07:00')
	assert (close_time(550, 600, start_time) == '2020-05-15T12:40:00-07:00')
	assert (close_time(890, 1000, start_time) == '2020-05-16T17:23:00-07:00')


def test_example_2():
	"""
	Consider a 1000km brevet with controls at 50km, 200km, 400km, 550km, 609km, 888km, 951km and 1067km (finish).
	This is an example off the top of my head, ran through the current time calculator located here:
	https://rusa.org/octime_acp.html
	"""
	start_time = arrow.get('2020-10-10T06:00:00-07:00')

	assert (open_time(0, 1000, start_time) == '2020-10-10T06:00:00-07:00')
	assert (open_time(60, 1000, start_time) == '2020-10-10T07:46:00-07:00')
	assert (open_time(200, 1000, start_time) == '2020-10-10T11:53:00-07:00')
	assert (open_time(400, 1000, start_time) == '2020-10-10T18:08:00-07:00')
	assert (open_time(550, 1000, start_time) == '2020-10-10T23:08:00-07:00')
	assert (open_time(609, 1000, start_time) == '2020-10-11T01:07:00-07:00')
	assert (open_time(800, 1000, start_time) == '2020-10-11T07:57:00-07:00')
	assert (open_time(1079, 1000, start_time) == '2020-10-11T15:05:00-07:00')

	assert (close_time(0, 1000, start_time) == '2020-10-10T07:00:00-07:00')
	assert (close_time(60, 1000, start_time) == '2020-10-10T10:00:00-07:00')
	assert (close_time(200, 1000, start_time) == '2020-10-10T19:20:00-07:00')
	assert (close_time(400, 1000, start_time) == '2020-10-11T08:40:00-07:00')
	assert (close_time(550, 1000, start_time) == '2020-10-11T18:40:00-07:00')
	assert (close_time(609, 1000, start_time) == '2020-10-11T22:47:00-07:00')
	assert (close_time(800, 1000, start_time) == '2020-10-12T15:30:00-07:00')
	assert (close_time(1079, 1000, start_time) == '2020-10-13T09:00:00-07:00')